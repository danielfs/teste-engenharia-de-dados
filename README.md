# Engenharia de Dados na DataSprints

## Introdução

Este repositório registra a minha solução no teste técnico para a vaga de
Engenharia de Dados na DataSprints.

O diretório `dados` contém os arquivos originais recebidos para o
desenvolvimento da solução.

## Quesitos

O desafio é composto de dois tipos de quesitos, os **mínimos** e os **bônus**. Cada
qual com suas questões específicas que, a partir dos dados oferecidos, deve-se
buscar respondê-los com exatidão.

### Quesitos mínimos

1. Qual a distância média percorrida por viagens com no máximo 2 passageiros;
2. Quais os 3 maiores *vendors* em quantidade total de dinheiro arrecadado;
3. Faça um histograma da distribuição mensal, nos 4 anos, de corridas pagas em
dinheiro;
4. Faça um gráfico de série temporal contando a quantidade de gorjetas de cada dia,
nos últimos 3 meses de 2011.

### Quesitos bônus

1. Qual o tempo médio das corridas nos dias de semana;
2. Fazer uma visualização em mapa com latitude e longitude de *pickups and dropoffs*
no ano de 2010;
3. Simular um streaming dos dados dos JSON e fazer uma visualização
acompanhando uma métrica em tempo-real;
4. Conseguir provisionar todo seu ambiente em uma cloud pública, de preferência
AWS;
5. Implementação de alguma arquitetura de pipeline de engenharia de dados para suas
análises.

## Entrega da solução

O artefato final para avaliação é este repositório no Gitlab, constituído em 3 partes:

- Este arquivo `README.md` que descreve sucintamente o desafio técnico, e mostra instruções de como reproduzir o código desenvolvido.
- O diretório `analises`, que contém um arquivo em PDF com as respostas das questões.
- O código-fonte desenvolvido durante o teste técnico.
